#include "mbed.h"

void addOne(int i) {
  i++; // Has no effect outside this scope because this is a copy of the original
}

/* void addOne(int& i) {
  i++; // Actually changes the original variable
}*/

void addOne(int* i) {
  (*i)++; // Actually changes the original variable, but i can be null ! 
}

// main() runs in its own thread in the OS
int main()
{
  int i = 0;

  addOne(i);
  printf("value of i is %d\n", i);

  addOne(&i);
  printf("value of i is %d\n", i);
}

